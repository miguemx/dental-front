import React from "react";
import ReactModal from 'react-modal';

import { Calendario, EdicionCita } from "../components";

export default class AgendaCalendarioScreen extends React.Component {

    state = {
        modal: false,
        startDate: null,
        endDate: null,
        lastId: null,

        agenda: []
    }

    /**
     * 
     * @param {date} startDate 
     * @param {date} endDate 
     */
    agregaCita = (startDate, endDate) => {
        this.setState({ modal: true, startDate: startDate, endDate: endDate });
    }

    /**
     * oculta el modal y actualiza el estado para refrescar el calendario
     * @param {string} lastId ultimo ID del acita
     */
    finalizaAgregaCita = (lastId) => {
        console.log(lastId);
        this.setState({ modal: false, lastId: lastId})
    }
    
    render() {
        
        return(
            <div className="pantalla-area-container">
                <div className="menu-seccion">
                    <div className="menu-seccion-encabezado">Agenda</div>
                </div>
                <div className="area-trabajo-container">
                    <Calendario agregaCita={this.agregaCita} ultimo={this.state.lastId} />                    
                </div>
                <ReactModal 
                    isOpen={this.state.modal} 
                    ariaHideApp={false} 
                    className="modal-content" 
                    overlayClassName="modal-overlay" 
                >
                    <EdicionCita 
                        cancel={()=>this.setState({ modal: false})} 
                        exito={this.finalizaAgregaCita} 
                        inicio={this.state.startDate}
                        fin = {this.state.endDate}
                        cita={null}
                    />
                </ReactModal>
            </div>
        );
    }

}