export { default as LoginScreen } from './login';

export { default as PacientesScreen } from './pacientes';
export { default as PrincipalScreen } from './principal';
export { default as AgendaScreen } from './agenda';
export { default as PresupuestosScreen } from "./presupuestos";

export { default as PacientesListadoScreen } from './pacienteslistado';
export { default as PacientesEdicionScreen } from './pacientesedicion';

export { default as PresupuestosListadoScreen } from "./presupuestoslistado";
export { default as PresupuestosEdicionScreen } from "./presupuestosedicion";

