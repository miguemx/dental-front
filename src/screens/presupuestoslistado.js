import React from "react";
import NumberFormat from 'react-number-format';
import {
    toApi,
    UrlListaPresupuestos
} from "../services/constants";

export default class PresupuestosListadoScreen extends React.Component {

    state = {
        presupuestos: []
    }

    componentDidMount() {
        if( typeof(this.props.params) !== 'undefined' ) {
            if( typeof(this.props.params.paciente) !== 'undefined' ) {
                this.obtenerPresupuestos( this.props.params.paciente );
            }
            else {
                this.obtenerPresupuestos(null);
            }
        }
        else {
            this.obtenerPresupuestos(null);
        }
    }
    
    /**
     * envia una peticion a la API para obtener la lista de los pacientes
     * y coloca en el estado los datos de ellos
     * @param {stirng} pacienteId el Id del paciente para buscar sus presupuestos generados (null si no se requiere)
     */
    obtenerPresupuestos = async (pacienteId) => {
        let data = { pagina: 1, registros: 25 };
        if ( pacienteId !== null ) {
            data.filtros = { paciente: pacienteId };
        }
        let response = await toApi( UrlListaPresupuestos, 'post', data );
        if ( response.status === 'ok' ) {
            this.setState({ presupuestos: response.data});
        }
    }

    /**
     * invoca al formulario para editar un presupuesto
     * @param {string} presupuestoId 
     */
    edita = (presupuestoId) => {
        // TODO
        console.log( "Editar el presupuesto" );
        console.log(presupuestoId);
        alert("Por el momento no esta disponible");
    }

    /**
     * llama a la api para eliminar un presupuesto
     * @param {string} presupuestoId 
     */
    elimina = (presupuestoId) => {
        // TODO
        console.log( "Eliminar el presupuesto" );
        console.log(presupuestoId);
        alert("Por el momento no esta disponible");
    }

    render() {
        return(
            <div className="pantalla-area-container">
                <div className="menu-seccion">
                    <div className="menu-seccion-encabezado">Presupuestos</div>
                    <button className="menu-seccion-item button-add" onClick={() => this.props.navega('edicion')}>
                        Agregar
                    </button>
                </div>
                <div className="area-trabajo-container">
                    <div className="tabla-datos-container">
                        <table className="tabla-datos">
                            <thead>
                                <tr>
                                    <th>Concepto</th>
                                    <th>Paciente</th>
                                    <th>Fecha</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.presupuestos.map( (item) => 
                                        <tr key={item.id}>
                                            <td className="registro-small">
                                                <div className="dato-principal">{item.nombre}</div>
                                                <div className="dato-secundario">{item.descripcion}</div>
                                            </td>
                                            <td className="registro-small">
                                                {item.paciente.nombre + ' ' + item.paciente.apellido}
                                            </td>
                                            <td className="registro-small">{item.fecha}</td>
                                            <td className="registro-small">{item.status}</td>
                                            <td className="registro-small" style={{textAlign: "right"}}>
                                                <NumberFormat
                                                    value={item.total} 
                                                    displayType={'text'} 
                                                    thousandSeparator={true} 
                                                    prefix={'$'} 
                                                    decimalSeparator="." 
                                                    decimalScale={2} fixedDecimalScale={true} 
                                                />
                                            </td>
                                            <td>
                                                <div className="tools-container">
                                                    <button className="menu-seccion-item button-pagos" 
                                                        onClick={ () => this.edita(item.id)}>
                                                        Ver
                                                    </button>

                                                    <button className="menu-seccion-item button-edit" 
                                                        onClick={ () => this.edita(item.id)}>
                                                        Editar
                                                    </button>
                                                    
                                                    <button className="menu-seccion-item button-delete" 
                                                        onClick={ () => this.elimina(item.id)}>
                                                        Eliminar
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        );
    }
}