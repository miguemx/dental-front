import React from "react";

import { EdicionPresupuesto } from "../components"

export default class PresupuestosEdicionxScreen extends React.Component {
    render() {
        let paciente = null;
        return(
            <div className="pantalla-area-container">
                <div className="menu-seccion">
                    <div className="menu-seccion-encabezado">Presupuestos</div>
                </div>
                <EdicionPresupuesto
                    cancel={ () => this.props.navega('listado') }
                    exito={ ()=>this.props.navega('listado') }
                    pacienteId={paciente}
                />
            </div>
        );
        
    }
}