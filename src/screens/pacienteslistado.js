import React from 'react';
import { 
    UrlListaPacientes, 
    UrlPFotos,  
    fotoPacienteGenerica, 
    toApi, 
    UrlEliminaPaciente 
} from "../services/constants";
import '../styles/TablaDatos.css';

class PacientesListadoScreen extends React.Component {

    state = {
        pacientes: [],
        nombre:"",
        apellido:"",
    }

    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        let medico = localStorage.getItem('medico');
        let filtros = { medicoCabecera: medico };
        this.obtienePacientes(filtros);
    }

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
            [name]: value
        });
    }

    /**
     * Verifica las teclas que se presionan en cada input que se indique este metodo
     * @param {object} e 
     */
    handleKey = (e) => {
        if (e.key === 'Enter') {
            this.busca();
        }
    }

    /**
     * envia una peticion a la api para obtener la lista de pacientes
     * @param filtros el objeto con los filtros que se deben mandar a la API
     */
    obtienePacientes = async (filtros) => {
        
        let data = { pagina: 1, registros: 25, filtros: filtros };
        let response = await toApi( UrlListaPacientes, 'post', data );
        if ( response.status === 'ok' ) {
            this.setState({ pacientes: response.data.registros });
        }
    }

    /**
     * genera los parametros para buscar pacientes a traves de la API
     * @param {bool} buscar la bandera indica si se debe realizar busqueda o solo listar todos los registros
     */
    busca = (buscar=true) => {
        let medico = localStorage.getItem('medico');
        let filtros = { medicoCabecera: medico };
        if ( buscar ) {
            if ( this.state.nombre.length > 0 ) filtros.nombre = this.state.nombre;
            if ( this.state.apellido.length > 0 ) filtros.apellido = this.state.apellido;
        }
        this.obtienePacientes(filtros);
    }

    /**
     * abre la pantalla de edicion de pacientes
     * @param {string} id el ID del paciente a editar
     */
    editaPaciente = (id) => {
        let params = {
            id: id
        }
        this.props.navega('edicion',params);
    }

    /**
     * abre la ventana para ver el expediente de un paciente
     * @param {string} id el ID del paciente para ver su expediente
     */
    verExpediente = (id) => {
        console.log("ver el expediente de " + id);
        let params = {
            seccion: 'expedientes',
            pantalla: 'listado',
            pacienteId: id,
        };
        this.props.navega( null, params );
    }

    /**
     * abre la ventana para ver o agregar los pagos y presupuestos de un paciente
     * @param {string} id  el ID del paciente
     */
    verPagos = (id) => {
        let params = {
            seccion: 'presupuestos',
            pantalla: 'listado',
            paciente: id,
        };
        this.props.navega( null, params );
    }

    /**
     * navega a la ventana de agenda 
     * @param {string} id el ID del paciente
     */
    agenda = (id) => {
        let params = {
            seccion: 'agenda',
            pantalla: 'edicion',
            paciente: id,
        };
        this.props.navega( null, params );
    }

    /**
     * llama a la api para eliminar un paciente
     * @param {string|int} id el ID del paciente (persona) a eliminar
     * @param {string} nombre el nombre del paciente a eliminar (solo informativo)
     */
    elimina = async (id, nombre) => {
        let mensaje = "Está seguro que desea eliminar el paciente " + nombre + "? \n\nAl eliminarlo perderá el acceso a su expediente y toda su información."; 
        if ( window.confirm(mensaje) ) {
            if ( window.confirm("Por favor confirme que desea eliminar al paciente " + nombre) ) {
                let data = { id: id };
                let response = await toApi( UrlEliminaPaciente, 'post', data );
                if ( response.status === 'ok' ) {
                    this.obtienePacientes();
                }
            }
        }
    }

    render() {
        return(
            <div className="pantalla-area-container">
                <div className="menu-seccion">
                    <div className="menu-seccion-encabezado">Pacientes</div>
                    <button className="menu-seccion-item button-add" onClick={() => this.props.navega('edicion')}>
                        Agregar
                    </button>
                </div>
                <div className="area-trabajo-container">
                    <div className="buscador-container">
                        <input type="text" name="nombre" value={this.state.nombre} placeholder="Nombre" 
                            onChange={this.handleInputChange} onKeyDown={this.handleKey} />
                        <input type="text" name="apellido" value={this.state.apellido} placeholder="Apellido" 
                            onChange={this.handleInputChange} onKeyDown={this.handleKey} />
                        <div className="action-button-container"><button className="button-search" onClick={()=>this.busca()}>Buscar</button></div>
                        <div className="action-button-container"><button className="button-list" 
                            onClick={()=>{this.setState({nombre: "", apellido:""});this.busca(false);}}>Todos</button></div>
                    </div>
                    <div className="tabla-datos-container">
                        <table className="tabla-datos">
                            <thead>
                                <tr>
                                    <th>Foto / # Expediente</th>
                                    <th>Nombre</th>
                                    <th>Celular</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.pacientes.map( (item) => 
                                        <tr key={item.id}>
                                            <td className="registro-foto">
                                                <img src={ (item.foto !== null && item.foto !== "")? UrlPFotos + item.foto: fotoPacienteGenerica } className="foto-listado" alt="" />
                                                <br />
                                                {item.expediente}
                                            </td>
                                            <td>
                                                <div className="dato-principal">{item.nombre}</div>
                                                <div className="dato-secundario">{item.apellido}</div>
                                            </td>
                                            <td className="registro-small">{item.celular}</td>
                                            <td>
                                                <div className="tools-container">
                                                    <button className="menu-seccion-item button-edit" onClick={ () => this.editaPaciente(item.id)}>Editar</button>
                                                    <button className="menu-seccion-item button-expediente" onClick={ () => this.verExpediente(item.id)}>Expediente</button>
                                                    <button className="menu-seccion-item button-calendario" onClick={ () => this.agenda(item.id)}>Agenda</button>
                                                    <button className="menu-seccion-item button-pagos" onClick={ () => this.verPagos(item.id)}>Presupuestos</button>
                                                    <button 
                                                        className="menu-seccion-item button-delete" 
                                                        onClick={ () => this.elimina(item.id, item.nombre+' '+item.apellido)}>
                                                            Eliminar
                                                    </button>
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    )
                                }
                                {
                                    (this.state.pacientes.length===0)?<tr><td colSpan={4}>No se encontraron Pacientes</td></tr>:null
                                }
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                
            </div>
            
        );
    }
}

export default PacientesListadoScreen;