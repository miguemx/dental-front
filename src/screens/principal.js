import React from 'react';

import { Menu } from '../components';

import PacientesScreen from './pacientes'
import AgendaScreen from './agenda'
import EsperaScreen from './espera';
import PresupuestosScreen from "./presupuestos";
import Expedientes from "./expedientes";

import '../styles/App.css';

class PrincipalScreen extends React.Component {
    state = {
        id: '',
        seccion: null,
        params: {}
    }

    componentDidMount() {
        this.setState({ id: 's'});
    }

    handleMenu = (seccion, params) => {
        this.setState({ seccion, params });
    }

    render() {
        let seccionSeleccionada = this.props.seccion;
        let seccion = <EsperaScreen />;

        if ( this.state.seccion !== null ) {
            seccionSeleccionada = this.state.seccion;
        }

        switch ( seccionSeleccionada ) {
            case 'pacientes':       seccion = <PacientesScreen handleMenu={this.handleMenu} params={this.state.params} />;         break;
            case 'agenda':          seccion = <AgendaScreen handleMenu={this.handleMenu} params={this.state.params} />;            break;
            case 'presupuestos':    seccion = <PresupuestosScreen handleMenu={this.handleMenu} params={this.state.params} />;      break;
            case 'expedientes':     seccion = <Expedientes handleMenu={this.handleMenu} params={this.state.params} />;             break;
            default:                seccion = <EsperaScreen />;                                                                    break;
        }
        
        return(
            <div className="main-container">
                <Menu handleMenu={this.handleMenu} selected={seccionSeleccionada} sesion={this.props.sesion} />
                {seccion}
                <div className="footer-container">
                    <span className="app-name">Sistema de Gestión Dental</span>
                    <span className="version-name">Version 0.9</span>
                </div>
            </div>
        );
    }
}

export default PrincipalScreen;