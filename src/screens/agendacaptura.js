import React from "react";
import { Calendario } from '../components';


export default class AgendaCapturaScreen extends React.Component {

    state = {
        agenda: []
    }

    componentDidMount() {
        
    }
    
    render() {
        
        return(
            <div className="pantalla-area-container">
                <div className="area-trabajo-container">
                    <Calendario />                   
                </div>
            </div>
        );
    }
}