import React from "react";
import PresupuestosEdicionScreen from "./presupuestosedicion";
import PresupuestosListadoScreen from "./presupuestoslistado";

export default class PresupuestosScreen extends React.Component {
    state = {
        id: '',
        pantalla: '',
        params: null
    }

    /**
     * cambia a otra pantalla, ya sea dentro de la misma seccion o de otra seccion
     * @param {string} pantalla el nombre de la pantalla de las seccion o null si se desea navegar a otra seccion
     * @param {object} params los parametros necesitados por la otra pantalla
     */
    cambiaPantalla = (pantalla, params) => {
        if ( pantalla !== null ) { // pantalla dentro de la misma seccion
            this.setState( { pantalla, params });
        }
        else { // pantalla en otra seccion
            this.props.handleMenu(params.seccion , params);
        }
    }

    render() {
        let pantalla = <PresupuestosListadoScreen navega={this.cambiaPantalla} params={this.props.params}  />
        switch ( this.state.pantalla ) {
            case 'listado': pantalla = <PresupuestosListadoScreen navega={this.cambiaPantalla} params={this.props.params} />; break;
            case 'edicion': pantalla = <PresupuestosEdicionScreen navega={this.cambiaPantalla} params={this.props.params} />; break;
            default: break;
        }
        return(
            <div className="pantalla-container">
                {pantalla}
            </div>
        );
    }
}