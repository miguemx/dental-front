import React from "react";

import {
    VisitasPaciente, PagosPaciente
} from "../components";

import {
    toApi, UrlVerPaciente, UrlPFotos, UrlAgendaPaciente, UrlPagosSaldo
} from "../services/constants";

import "../styles/Expediente.css";

export default class Expedientes extends React.Component {

    state = {
        paciente: {
            expediente: "Cargando...",
            nombre: "Cargando...",
            apellido: "Cargando...",
            sexo: "",
            fechaNacimiento: "",
            celular: "",
            alertas: "",
        },
        visitas: null,
        saldo: null,
        seccion: "visitas"
    }

    componentDidMount() {
        if( typeof(this.props.params) !== 'undefined' ) {
            if ( this.props.params.pacienteId !== 'undefined' ) {
                this.getPerfil(this.props.params.pacienteId);
                this.getVisitas(this.props.params.pacienteId);
                this.getSaldo( this.props.params.pacienteId );
            }
        }
    }

    /**
     * obtiene la información general del paciente
     * @param {string} pacienteId el ID del paciente a buscar
     */
    getPerfil = async (pacienteId) => {
        let response = await toApi( UrlVerPaciente + pacienteId, 'GET', null );
        if ( response ) {
            this.setState({ paciente: response.data });
        }
    }

    /**
     * obtiene las visitas
     * @param {string} pacienteId el ID del paciente
     */
    getVisitas = async(pacienteId) => {
        let response = await toApi( UrlAgendaPaciente + pacienteId, 'GET', null );
        if ( response ) {
            this.setState({visitas: response.data});
        }
    }
    
    /**
     * obtiene el saldo desde la API del paciente en cuestion
     * @param {string} pacienteId 
     */
    getSaldo = async( pacienteId ) => {
        let response = await toApi( UrlPagosSaldo + pacienteId, 'GET', null );
        if ( response ) {
            this.setState({ saldo: response.data});
        }
    }

    render() {
        let seccion = null;
        switch ( this.state.seccion ) {
            case 'visitas': seccion = <VisitasPaciente visitas={this.state.visitas} />; break;
            case 'pagos': seccion = <PagosPaciente saldo={this.state.saldo} paciente={this.props.params.pacienteId} />; break;
            default: break;
        }
        return(
            <div className="pantalla-container expediente-container">
                <div className="expediente-header">
                    { (this.state.paciente.foto) ? <img src={UrlPFotos + this.state.paciente.foto} alt="Foto de Paciente" className="paciente-foto" /> : null }
                    
                    <div className="paciente-data">
                        <div className="expediente-title-nombre">{this.state.paciente.nombre} {this.state.paciente.apellido}</div>
                        <div className="expediente-title-expediente">Expediente {this.state.paciente.expediente}</div>
                        <div>{this.state.paciente.sexo} | {this.state.paciente.fechaNacimiento} | {this.state.paciente.celular} | {this.state.paciente.id}</div>
                        {
                            (this.state.paciente.alertas !== "")?
                            <div className="expediente-title-alertas">{this.state.paciente.alertas}</div>
                            : null
                        }
                    </div>
                </div>
                <div className="expediente-menu">
                    <button onClick={()=>this.setState({seccion: "visitas"})}>Visitas</button>
                    <button onClick={()=>this.setState({seccion: "pagos"})}>Pagos</button>
                </div>
                <div className="expediente-trabajo-container">
                    {seccion}
                </div>
            </div>
        );
    }

}