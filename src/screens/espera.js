import React from 'react';

class EsperaScreen extends React.Component {
    render() {
        return(
            <div>
                Espere un momento por favor.
            </div>
        );
    }
}

export default EsperaScreen;