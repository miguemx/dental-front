import React from 'react';

import AgendaCapturaScreen from "./agendacaptura";
import AgendaCalendarioScreen from "./agendacalendario";

export default class AgendaScreen extends React.Component {
    state = {
        id: '',
        pantalla: '',
        params: null
    }

    componentDidMount() {
        // if( this.props.params.pantalla ) {
        //     console.log( this.props.params );
        //     this.setState( { pantalla: this.props.params.pantalla} );
        // }
    }

    /**
     * cambia a otra pantalla, ya sea dentro de la misma seccion o de otra seccion
     * @param {string} pantalla el nombre de la pantalla de las seccion o null si se desea navegar a otra seccion
     * @param {object} params los parametros necesitados por la otra pantalla
     */
    cambiaPantalla = (pantalla, params) => {
        if ( pantalla !== null ) { // pantalla dentro de la misma seccion
            this.setState( { pantalla, params });
        }
        else { // pantalla en otra seccion
            this.props.handleMenu(params.seccion , params);
        }
    }

    render() {
        let pantalla = <AgendaCalendarioScreen navega={this.cambiaPantalla}  />
        switch ( this.state.pantalla ) {
            case 'listado': pantalla = <AgendaCalendarioScreen navega={this.cambiaPantalla} params={this.state.params} />; break;
            case 'edicion': pantalla = <AgendaCapturaScreen navega={this.cambiaPantalla} params={this.state.params} />; break;
            default: break;
        }
        return(
            <div className="pantalla-container">
                {pantalla}
            </div>
        );
    }
}
