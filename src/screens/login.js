import React from 'react';

class LoginScreen extends React.Component {

    state = {
        usuario: '',
        contrasena: '',
        iniciando: false,
    }

    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
            [name]: value
        });
    }

    /**
     * actualiza el comportamiento para el inicio de sesion
     */
    iniciarSesion = async () => {
        this.setState({iniciando: true});
        await this.props.handleLogin(this.state.usuario, this.state.contrasena);
        this.setState({iniciando: false});
    }

    handleKey = (e) => {
        if (e.key === 'Enter') {
            this.iniciarSesion();
        }
    }

    render() {
        return(
            <div className="main-container">
                <div className="login-container">
                    <div className="login-form">
                        <p className="jumbotron">Sistema de Gesti&oacute;n Dental</p>
                        <p className="text-indicacion">Por favor introduce tus datos para acceder a la plataforma</p>
                        <label>Usuario</label>
                        <input 
                            type="text" 
                            name="usuario" 
                            placeholder="Nombre de usuario" 
                            onChange={this.handleInputChange} 
                            value={this.state.usuario} 
                            onKeyDown={this.handleKey}
                        />
                        <label>Contraseña</label>
                        <input 
                            type="password" 
                            name="contrasena" 
                            placeholder="Contraseña" 
                            onChange={this.handleInputChange} 
                            value={this.state.contrasena} 
                            onKeyDown={this.handleKey}
                        />
                        {
                            (this.state.iniciando === false)?
                            <button onClick={this.iniciarSesion} className="big-button">Iniciar Sesi&oacute;n</button>
                            :
                            <button className="big-button-disabled">Iniciando Sesi&oacute;n</button>
                        }
                        {
                            (this.props.errorReported !== null )?
                            <div className="message-error">
                                {this.props.errorReported}
                            </div>
                            :
                            <div></div>
                        }
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default LoginScreen;