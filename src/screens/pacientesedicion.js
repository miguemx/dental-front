import React from 'react';
import {  EdicionPaciente } from '../components';

class PacientesEdicionScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    
    }

    componentDidMount() {
        
    }
    
    render() {
        let paciente = null;
        if ( typeof(this.props.params) === 'object' ) {
            paciente = this.props.params.id;
        }
        return(
            <div className="pantalla-area-container">
                <div className="menu-seccion">
                    <div className="menu-seccion-encabezado">Edici&oacute;n de Pacientes</div>
                </div>
                <EdicionPaciente 
                    cancel={ () => this.props.navega('listado') }
                    exito={ ()=>this.props.navega('listado') }
                    pacienteId={paciente}
                />
            </div>
        );
    }
}

export default PacientesEdicionScreen;