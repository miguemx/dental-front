import React from 'react';

import { LoginScreen, PrincipalScreen } from './screens';

import { UrlLogin, UrlToken, getHeaders } from "./services/constants";

class App extends React.Component {

	state = {
		session: null,
		pantallaSeleccionada: '',
		errorReported: null
	}

	componentDidMount() {
		this.validaSesion();
	}

	validaSesion = async () => {
		let responseObject;
		let token = localStorage.getItem('tk');
		let data = { token: token };
		let response = await fetch(UrlToken, {
			method: 'POST', 
			body: JSON.stringify(data), 
			headers:getHeaders(token)
		}).then( (res) =>{ responseObject = res; return res.json() } )
		.catch(error => console.error('Error:', error)).then( (response) => { return response } );
		
		if ( typeof(response) == 'object' ) {
			if ( response.status === 'ok' ) {
				localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
				this.setState({ 
					session: response.data.data,
					pantallaSeleccionada: response.data.data.pantalla,
				});
			}
		}
		else {
			console.log("ERROR PARSING RESPONSE");
		}
	}

	/**
	 * llama a la API para realizar un inicio de sesion
	 * @param {string} username la cadena con el nombre de usuario
	 * @param {string} password la cadena con la contrasena
	 */
	iniciaSesion = async (username, password) => {
		let data = { username: username, password: password};
		let responseObject;
		let response = await fetch(UrlLogin, {
			method: 'POST', 
			body: JSON.stringify(data), 
			headers:{
				'Content-Type':'application/json',
				'Accept':  '*/*'
			}
		}).then( (res) => { responseObject=res; return res.json() } ).catch(error => console.error('Error:', error))
		.then( (response) => { return response } );
		
		if ( typeof(response) == 'object' ) {
			if ( response.status === 'ok' ) {
				await localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
				await localStorage.setItem( 'medico', response.data.id );
				this.setState({ 
					session: response.data,
					pantallaSeleccionada: response.data.pantalla,
				});
			}
			else {
				let message = response.message;
				if ( response.data ) {
					if ( response.data.length > 0 ) {
						for ( let i=0; i<response.data.length; i++ ) {
							message += "\n\r" + response.data[i];
						}
					}
				}
				this.setState({errorReported: message});
			}
		}
		else {
			console.log("ERROR PARSING RESPONSE");
		}
	}

	render() {
		if ( this.state.session != null) {
			return(
				<PrincipalScreen seccion={this.state.pantallaSeleccionada} sesion={this.state.session} />
			);
		}
		else {
			return(
				<LoginScreen handleLogin={this.iniciaSesion} errorReported={this.state.errorReported} />
			);
		}
	}
}



export default App;
