import React from "react";
import NumberFormat from "react-number-format";
import { 
    UrlPagosSaldo, 
    toApi,
    UrlPresupuestosVer,
    UrlPresupuestosEditar
} from "../services/constants";
import EdicionPagos from "./edicionpagos";

export default class PagosPaciente extends React.Component {

    state = {
        saldo: { 
            deuda: 0,
            pagado: 0,
            saldo: 0,
            detalle: { presupuestos: [], pagos: [] } 
        },
        paciente: null,
        medico: null,
        presupuesto: null,
    }

    componentDidMount() {    
        let medico = localStorage.getItem("medico");
        this.setState({
            saldo: this.props.saldo,
            paciente: this.props.paciente,
            medico: medico
        });
    }

    /**
     * genera una lista renderizable con todos los presupuestos del paciente
     */
    listaPresupuestos = () => {
        return(
            <div className="expedientes-contenedor-lista">
                <div className="expedientes-titulo-lista">Presupuestos</div>
                {this.state.saldo.detalle.presupuestos.map( (item) =>  
                    <button key={item.id} onClick={() => this.seleccionaPresupuesto(item)}>
                        <div>{item.nombre}</div>
                        <div>{item.status}</div>
                        <div className="expediente-saldo">
                            <NumberFormat
                                value={item.total} 
                                displayType={'text'} 
                                thousandSeparator={true} 
                                prefix={'$ '} 
                                decimalSeparator="." 
                                decimalScale={2} fixedDecimalScale={true} 
                            />
                        </div>
                    </button>
                )}
                {
                    (this.state.saldo.detalle.presupuestos.length < 1)?<span>No Hay registros</span>:null
                }
            </div>
        );
    }

    /**
     * genera una lista renderizable con todos los pagos que ha realizado el paciente
     */
    listaPagos = () => {
        return(
            <div className="expedientes-contenedor-lista">
                <div className="expedientes-titulo-lista">Pagos</div>
                {this.state.saldo.detalle.pagos.map( (item) =>  
                    <button key={item.id}>
                        <div>{item.concepto}</div>
                        <div>{item.fechaPago}</div>
                        <div className="expediente-saldo">
                            <NumberFormat
                                value={item.total} 
                                displayType={'text'} 
                                thousandSeparator={true} 
                                prefix={'$ '} 
                                decimalSeparator="." 
                                decimalScale={2} fixedDecimalScale={true} 
                            />
                        </div>
                    </button>
                )}
                {
                    (this.state.saldo.detalle.pagos.length < 1)?<span>No Hay registros</span>:null
                }
            </div>
        );
    }

    /**
     * genera un item renderizable con el saldo a mostrar del cliente
     * @param {sting} params las propuedades tipo=>el tipo de despliegue valor=>el numero a escribir
     */
    saldos = (params) => {
        let clase = "";
        let titulo = ""
        switch(params.tipo) {
            case "deuda": clase="expediente-deuda"; titulo="Presupuestos Aprobados"; break;
            case "pagos": clase="expediente-pago"; titulo="Pagos Realizados"; break;
            case "saldo": clase="expediente-saldo"; titulo="Saldo del Paciente"; break;
            default: break;
        }
        return(
            <div className={clase}>
                {titulo + " - "}
                <NumberFormat
                    value={params.valor} 
                    displayType={'text'} 
                    thousandSeparator={true} 
                    prefix={'$ '} 
                    decimalSeparator="." 
                    decimalScale={2} fixedDecimalScale={true} 
                />
            </div>
        );
    }

    /**
     * Muestra el detalle de un presupuesto seleccionado
     * @param {string} props.presupuesto el presupuesto a obtener
     */
    detallePresupuestos = (props) => {
        if ( props.presupuesto === null ) {
            return <div className="expediente-panel">
                <div className="expedientes-titulo-lista">Detalle del Presupuesto</div>
                <em>Seleccione un presupuesto del panel izquierdo</em>
            </div>;
        }
        else {
            return (
                <div className="expediente-panel">
                    <div className="expedientes-titulo-lista">Detalle del Presupuesto</div>
                    <div className="expediente-titulo-lista">
                        { (this.state.presupuesto.status === "APROBADO")? "APROBADO": 
                            <button onClick={ () => this.aprobarPresupuesto(this.state.presupuesto.id) }>Aprobar</button>
                        }
                    </div>
                    <table className="presupuesto-items">
                        <thead>
                            <tr>
                                <th>Concepto</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.presupuesto.items.map(
                                    (item) => 
                                        <tr key={item.id}>
                                            <td>{item.concepto}</td>
                                            <td style={{textAlign: "center"}}>{item.cantidad}</td>
                                            <td style={{textAlign: "right"}}>
                                                <NumberFormat
                                                    value={item.unitario} displayType={'text'} thousandSeparator={true} 
                                                    prefix={'$ '} decimalSeparator="." decimalScale={2} fixedDecimalScale={true} 
                                                />
                                            </td>
                                        </tr>
                                )
                            }
                            <tr>
                                <td colSpan={2}><strong>Subtotal</strong></td>
                                <td style={{textAlign: "right", fontWeight: "bold"}}>
                                    <NumberFormat
                                        value={this.state.presupuesto.subtotal} displayType={'text'} thousandSeparator={true} 
                                        prefix={'$ '} decimalSeparator="." decimalScale={2} fixedDecimalScale={true} 
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2}><strong>Impuesto</strong></td>
                                <td style={{textAlign: "right", fontWeight: "bold"}}>
                                    <NumberFormat
                                        value={this.state.presupuesto.impuesto} displayType={'text'} thousandSeparator={true} 
                                        prefix={'$ '} decimalSeparator="." decimalScale={2} fixedDecimalScale={true} 
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2}><strong>Total</strong></td>
                                <td style={{textAlign: "right", fontWeight: "bold"}}>
                                    <NumberFormat
                                        value={this.state.presupuesto.total} displayType={'text'} thousandSeparator={true} 
                                        prefix={'$ '} decimalSeparator="." decimalScale={2} fixedDecimalScale={true} 
                                    />
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            );
        }
    }

    /**
     * llama a la api despues de realizar un cambio para actualizar los saldos mostrados
     */
    actualizaSaldos = async () => {
        let response = await toApi( UrlPagosSaldo + this.props.paciente, 'get', null );
        if ( response ) {
            this.setState({ saldo: response.data });
        }
    }

    /**
     * Pone un presupuesto en el estado para poder dibujar la pantalla de edicion del mismo
     * @param {object} presupuesto el presupuesto seleccionado
     */
    seleccionaPresupuesto = async (presupuesto) => {
        console.log(presupuesto);
        let response = await toApi( UrlPresupuestosVer + presupuesto.id, 'get', null );
        if ( response ) {
            this.setState({ presupuesto: response.data });
            console.log(response.data);
        }
    }

    /**
     * Llama a la API para actualizar el presupuesto y enviarle la bandera de aprobado
     * @param {string} idPresupuesto el ID del presupuesto
     */
    aprobarPresupuesto = async (idPresupuesto) => {
        let data = { status: "APROBADO" };
        let response = await toApi( UrlPresupuestosEditar + idPresupuesto, 'post', data );
        if ( response ) {
            this.actualizaSaldos();
            alert("El presupuesto ha sido APROBADO.");
        }
    }

    render() {
        
        return(
            <div className="expediente-trabajo">
                <div className="expediente-items">
                    <this.listaPresupuestos />
                    <this.listaPagos />
                </div>
                <div className="expediente-area">
                    <div className="expediente-panel">
                        <div className="expedientes-titulo-lista expediente-saldo">Resumen</div>
                        <this.saldos tipo="deuda" valor={this.state.saldo.deuda} />
                        <this.saldos tipo="pagos" valor={this.state.saldo.pagado} />
                        <this.saldos tipo="saldo" valor={this.state.saldo.saldo} />
                    </div>
                    <EdicionPagos exito={this.actualizaSaldos} paciente={this.props.paciente} />
                    <this.detallePresupuestos presupuesto={this.state.presupuesto} />
                </div>
            </div>
        );
    }

}