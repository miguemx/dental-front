import React from "react";
import ReactModal from 'react-modal';

import { Calendar, momentLocalizer, Views } from 'react-big-calendar'
import moment from 'moment';

import { EdicionCita } from "../components";
import { UrlAgendaMedico, toApi, UrlAgendaActualiza } from "../services/constants";

import "react-big-calendar/lib/css/react-big-calendar.css"
import "../styles/Calendario.css";

require('moment/locale/es.js');
  
const localizer = momentLocalizer(moment);

export default class Calendario extends React.Component {

    state = {
        eventos:[
            
        ],
        modal: false,
        cita: null,
    }

    constructor(props) {
        super(props);
        this.wrapper = React.createRef();
    }

    componentDidMount() {
        this.initialRange();
    }

    componentDidUpdate() {
        // this.initialRange();
    }

    /**
     * obtiene el rango inicial al cargar el calendario para obtener las citas de ese rango
     */
    initialRange = () => {
        let fecha = new Date();
        let momentoInicial = moment(fecha);
        momentoInicial.subtract(1,"months");
        let momentoFinal = moment(fecha);
        momentoFinal.add(2,"months");
        let fechaInicio = momentoInicial.year() + '-' + ( (momentoInicial.month() < 10)? '0'+(momentoInicial.month()) : momentoInicial.month() ) + '-' +  ( (momentoInicial.date()<10)? '0'+momentoInicial.date() : momentoInicial.date() );
        let fechaFin = momentoFinal.year() + '-' + ( (momentoFinal.month() < 10)? '0'+(momentoFinal.month()) : momentoFinal.month() ) + '-' +  ( (momentoFinal.date()<10)? '0'+momentoFinal.date() : momentoFinal.date() );
        this.obtenerCitas( fechaInicio, fechaFin );
    }

    /**
     * dispara un modal para hacer operaciones sobre una cita en particular,
     * sobre la que el usuario da un clic
     * @param {object} event los datos de la cita
     */
    onEventClick = (event) => {
        this.setState({ modal: true, cita: event.data});
    }

    /**
     * dispara un modal para agregar una nueva cita, tomando como punto de partida
     * el slot en el que el usuario da clic
     * @param {object} slotInfo datos del slot seleccionado
     */
    onSlotChange = (slotInfo) => {
        this.props.agregaCita( slotInfo.start, slotInfo.end );
    }

    /**
     * muestra un nuevo rango en el calendario (mes, semana o dia) y construye la consulta para enviar la
     * peticion de citas a la api
     * @param {object} newRange arreglo u objeto con los datos del rango visible en el calendario
     */
    newRange = (newRange) => {
        let fecha = new Date();
        let fechaInicio = fecha.getFullYear() + '-' + ( (fecha.getMonth()+1 < 10)? '0'+(fecha.getMonth()+1) : fecha.getMonth()+1 ) + '-01';
        let fechaFin = fecha.getFullYear() + '-' + ( (fecha.getMonth()+1 < 10)? '0'+(fecha.getMonth()+1) : fecha.getMonth()+1 ) + '-28';
        if ( newRange.length ) {
            let datosFechaFin = null;
            let datosFechaInicio = newRange[0].toISOString().split('T');
            if ( newRange.length === 7 ) { // es la semana
                datosFechaFin = newRange[6].toISOString().split('T');
            }
            else { // es solo el dia
                datosFechaFin = newRange[0].toISOString().split('T');
            }
            fechaInicio = datosFechaInicio[0];
            fechaFin = datosFechaFin[0];
        }
        else {
            let datosFechaInicio = newRange.start.toISOString().split('T');
            let datosFechaFin = newRange.end.toISOString().split('T');
            fechaInicio = datosFechaInicio[0];
            fechaFin = datosFechaFin[0];
        }
        this.obtenerCitas( fechaInicio, fechaFin );
    }

    /**
     * obtiene desde la api las citas del medico para el rango actual del calendario
     * @param {string} fechaInicio string en formato mysql con la fecha de inicio a buscar
     * @param {string} fechaFin string en formato mysql con la fecha de inicio a buscar donde termina el rango
     */
    obtenerCitas = async (fechaInicio, fechaFin) => {
        let medico = localStorage.getItem('medico');
        let data = {
            inicio: fechaInicio,
            fin: fechaFin
        }
        let response = await toApi( UrlAgendaMedico + medico, "post", data );
        if ( response !== null ) {
            
            let eventos = response.data.map(
                (item) => {
                    return {
                        title: item.paciente.nombre + ' ' + item.paciente.apellido + " - " + item.motivo,
                        start: new Date(item.fecha),
                        end: new Date(item.fechaFin),
                        data: item
                    }
                }
            );
            this.setState({ eventos });
        }
    }

    eliminar = () => {
        console.log("ELIMINAD");
        console.log( this.state.cita );
        alert("Función aún no disponible");
    }

    registrar = (tipo) => {
        console.log("Registrar la cita");
        console.log( this.state.cita );
        tipo = tipo.toUpperCase();
        let response = toApi( UrlAgendaActualiza + this.state.cita.id, "post", { status: tipo } );
        if ( response.status !== null ) {
            this.setState({modal: false});
        }
    }

    /**
     * crea un recuadro personalizado para pintar las citas en el calendario, donde discrimina por estado
     * de la cita para poner un cintillo de color para identificar
     * @param {object} dataEvent datos de la cita
     */
    customEvent = (dataEvent) => {
        let clase = "cita-espera";
        switch(dataEvent.event.data.status) {
            case 'ATENDIDA': clase="cita-atendida"; break;
            case 'EN ESPERA': clase="cita-espera"; break;
            case 'CANCELADA': clase="cita-cancelada"; break;
            case 'FALTA': clase="cita-falta"; break;
            default: break;
        }
        return(
            <div className={clase}>
                <span>{dataEvent.event.title}</span>
            </div>
        );
    }
    
    render() {
        let defaultDate = new Date(); defaultDate.setHours(8); defaultDate.setMinutes(0);
        return(
            <div className="calendario-container">
                <Calendar
                    ref={this.wrapper}
                    localizer={localizer}
                    events={this.state.eventos}
                    startAccessor="start"
                    endAccessor="end"
                    style={{ height: "80vh" }}
                    step={30}
                    timeslots={1}
                    selectable={true}
                    onSelectEvent={event => this.onEventClick(event)}
                    onSelectSlot={(slotInfo) => this.onSlotChange(slotInfo) }
                    onRangeChange={ (range) => this.newRange(range) }
                    defaultView={Views.WEEK}
                    defaultDate={defaultDate}
                    views={['month', 'week', 'day']}
                    scrollToTime={defaultDate}
                    formats={{
                        timeGutterFormat: 'hh:mm A'
                    }}
                    components={{
                        event: this.customEvent
                    }}
                    messages={{
                        time: 'Hora',
                        event: 'Cita',
                        allDay: 'Todo el día',
                        week: 'Semana',
                        work_week: 'Semana laboral',
                        day: 'Día',
                        month: 'Mes',
                        previous: 'Anterior',
                        next: 'Siguiente',
                        yesterday: 'Ayer',
                        tomorrow: 'Mañana',
                        today: 'Hoy',
                        agenda: 'Agenda',
                        noEventsInRange: 'No hay citas en este rango.',
                        showMore: function(e) { return ' Ver ' + e + ' mas' }
                    }} />
                <ReactModal 
                    isOpen={this.state.modal} 
                    ariaHideApp={false} 
                    className="modal-content" 
                    overlayClassName="modal-overlay" 
                >
                    
                    <h3>Modificar datos de la cita</h3>
                    <EdicionCita 
                        cancel={null} 
                        exito={ () => {this.setState({modal: false})}} 
                        inicio={null}
                        fin = {null}
                        cita={this.state.cita}
                    />
                    <div class="bottom-button-container">
                        <button onClick={()=>this.registrar("atendida")} className="button-cita-check button-atender">Cita Atendida</button>
                        <button onClick={()=>this.registrar("falta")} className="button-cita-falta button-faltar">Paciente Falta</button>
                        <button onClick={()=>this.registrar("cancelada")} className="button-cita-cancela button-cancelar">Paciente Cancela</button>
                        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <button onClick={this.eliminar} className="button-delete">Eliminar</button>
                        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <button onClick={()=>this.setState({modal: false})} className="button-cancel">Cerrar</button>
                    </div>
                </ReactModal>
            </div>
        );
    }

}