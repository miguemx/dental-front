import React from "react";
import DatePicker from 'react-date-picker';
import { 
    fotoPacienteGenerica, 
    UrlCargaFotos, 
    UrlPFotos, 
    UrlAgregaPaciente, 
    getHeaders,
    UrlEditaPaciente,
    UrlVerPaciente,
    UrlListaMedicos, 
    toApi
} from "../services/constants";
import "../styles/Forms.css";
import "react-datepicker/dist/react-datepicker.css";


export default class EdicionPaciente extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: null,
            idPaciente: null,
            userfile: fotoPacienteGenerica,
            nombre: '',
            apellido: '',
            domicilio: '',
            correo: '',
            celular: '',
            telefono: '',
            documentoId: '',
            curp: '',
            edad: '',
            ciudad: '',
            estado: '',
            pais: '',
            foto: '',
            alertas: '',
            sexo: 'H',
            fechaNacimiento : new Date(),
            medicoCabecera: '',

            guardando: false,
            medicos: []
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        if( typeof(this.props.pacienteId) === "string" ) {
            this.getDataPaciente( this.props.pacienteId );
        }
        this.getMedicos();
    }

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
            [name]: value
        });
    }

    /**
     * ajusta el cambio de fecha en el control de calendario
     * @param {date} date la fecha seleccionada en el calendario
     */
    handleChangeFecha = date => {
        this.setState({
          fechaNacimiento: date
        });
    };

    /**
     * envia al servidor la foto colocada por el usuario en el control
     * @param {object} event el control de formulario que acepta archivos
     */
    uploadFoto = async (event) => {
        var formData = new FormData();
        formData.type = 'multipart/form-data';
        formData.set('userfile', event.target.files[0]);
        formData.append("userfile",event.target.files[0].name);
        formData.append("otramadre","event.target.files[0].name");

        let response = await fetch( UrlCargaFotos, {
            method: 'POST',
            
            body: formData
        })
        .then( response => response.json() )
        .catch(error => { console.log(error)})
        .then( (response) => { return response });

        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                this.setState({ 
                    userfile: UrlPFotos + response.data,
                    foto: response.data
                });
            }
        }
        else {
            console.log("RESPONSE:");
            console.log(response);
        }
    }

    /**
     * valida y guarda los datos capturados por el usuario
     */
    guarda = () => {
        this.setState({ guardando: true });
        if ( this.state.id === null ) {
            this.agrega();
        }
        else {
            this.actualiza();
        }
        
        this.setState( {guardando: false} );
    }

    
    /**
     * llama a la api para agregar un nuevo paciente a la base de datos
     */
    agrega = async () => {
        let data = this.state2Param();
        let token = localStorage.getItem('tk');
        let response = await fetch(UrlAgregaPaciente, {
			method: 'POST', 
			body: JSON.stringify(data), 
			headers:getHeaders(token)
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
        .then( (response) => { return response } );
        
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                alert( "Paciente creado con éxito." );
                this.props.exito();
            }
            else {
                if( response.message.startsWith("Valida") ) {
                    let message = "";
                    for ( let i=0; i<response.data.length; i++ ) {
                        message += "\n" + response.data[i];
                    }
                    alert( "Por favor corrija los siguientes errores:" + message );
                }
                else {
                    alert( response.message );
                }
            }
        }
    }

    /**
     * llama a la api para actualiza un paciente ya existente en la base de datos
     * 
     */
    actualiza = async () => {
        let data = this.state2Param();
        data.id = this.state.id;
        let token = localStorage.getItem('tk');
        let response = await fetch(UrlEditaPaciente, {
			method: 'POST', 
			body: JSON.stringify(data), 
			headers:getHeaders(token)
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
        .then( (response) => { return response } );
        
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                alert( "Paciente actualizado con éxito." );
                this.props.exito();
            }
            else {
                if( response.message.startsWith("Valida") ) {
                    let message = "";
                    for ( let i=0; i<response.data.length; i++ ) {
                        message += "\n" + response.data[i];
                    }
                    alert( "Por favor corrija los siguientes errores:" + message );
                }
                else {
                    alert( response.message );
                }
            }
        }
    }

    /**
     * llama a la api para buscar un paciente definido por su id
     * @param {string} id el ID del paciente a buscar
     * @deprecated
     */
    getPaciente = async (id) => {
        let responseObject;
        let token = localStorage.getItem('tk');
        let response = await fetch(UrlVerPaciente + id, {
			method: 'GET', 
			headers: getHeaders(token)
		}).then( (res) =>{ responseObject = res; return res.json() } )
		.catch(error => console.error('Error:', error)).then( (response) => { return response } );
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
                let foto = ( (response.data.foto !== null && response.data.foto.length !== 0 )? UrlPFotos+response.data.foto : fotoPacienteGenerica )
                let fechaData = response.data.fechaNacimiento;
                console.log( fechaData );
                this.setState({
                    id: response.data.id,
                    idPaciente: response.data.id,
                    userfile: foto,
                    nombre: response.data.nombre,
                    apellido: response.data.apellido,
                    domicilio: response.data.domicilio,
                    correo: response.data.correo,
                    celular: response.data.celular,
                    telefono: response.data.telefono,
                    documentoId: response.data.documentoId,
                    curp: response.data.curp,
                    edad: response.data.edad,
                    ciudad: (response.data.ciudad == null ) ? '' : response.data.ciudad,
                    estado: ( response.data.estado == null ) ? '' : response.data.estado,
                    pais: (response.data.pais==null)? '': response.data.pais,
                    foto: ( (response.data.foto !== null && response.data.foto.length !== 0 )? response.data.foto: fotoPacienteGenerica ),
                    alertas: (response.data.alertas == null)?'':response.data.alertas,
                    fechaNacimiento : new Date(),
                    medicoCabecera: ( response.data.medicoCabecera == null )? '': response.data.medicoCabecera ,
                });
            }
            else {
                console.log(response);
            }
        }
    }
    getDataPaciente = async (id) => {
        let response = await toApi( UrlVerPaciente + id, 'GET', null );
        if ( response.status === 'ok' ) {
            let foto = ( (response.data.foto !== null && response.data.foto.length !== 0 )? UrlPFotos+response.data.foto : fotoPacienteGenerica )
            let fechaData = response.data.fechaNacimiento.split("-");
            let fecha = new Date(fechaData[0], parseInt(fechaData[1])-1, fechaData[2]);
            console.log( fechaData );
            console.log( fecha );
            
            this.setState({
                id: response.data.id,
                idPaciente: response.data.id,
                userfile: foto,
                nombre: response.data.nombre,
                apellido: response.data.apellido,
                domicilio: response.data.domicilio,
                correo: response.data.correo,
                celular: response.data.celular,
                telefono: response.data.telefono,
                documentoId: response.data.documentoId,
                curp: response.data.curp,
                edad: response.data.edad,
                sexo: response.data.sexo,
                ciudad: (response.data.ciudad == null ) ? '' : response.data.ciudad,
                estado: ( response.data.estado == null ) ? '' : response.data.estado,
                pais: (response.data.pais==null)? '': response.data.pais,
                foto: ( (response.data.foto !== null && response.data.foto.length !== 0 )? response.data.foto: fotoPacienteGenerica ),
                alertas: (response.data.alertas == null)?'':response.data.alertas,
                fechaNacimiento : fecha,
                medicoCabecera: ( response.data.medicoCabecera == null )? '': response.data.medicoCabecera ,
            });
        }
    }

    /**
     * llama a la api para buscar un paciente definido por su id
     * @param {string} id el ID del paciente a buscar
     */
    getMedicos = async () => {
        let responseObject;
        let token = localStorage.getItem('tk');
        let data = { pagina: 1, registros: 9999 };
        let response = await fetch(UrlListaMedicos, {
            method: 'POST', 
            body: JSON.stringify(data), 
			headers: getHeaders(token)
		}).then( (res) =>{ responseObject = res; return res.json() } )
		.catch(error => console.error('Error:', error)).then( (response) => { return response } );
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
                this.setState({ medicos: response.data.registros });
            }
            else {
                alert( response.message );
            }
        }
    }


    /**
     * convierte el estado del componente en los parametros para guardar la informacion
     */
    state2Param = () => {
        let anio = this.state.fechaNacimiento.getFullYear();
        let mes = this.state.fechaNacimiento.getMonth() + 1;
        let dia = this.state.fechaNacimiento.getDate();
        if ( mes < 10 ) mes = '0' + mes;
        if ( dia < 10 ) dia = '0' + dia;
        let data = {
            nombre: this.state.nombre,
            apellido: this.state.apellido,
            domicilio: this.state.domicilio,
            correo: this.state.correo,
            celular: this.state.celular,
            telefono: this.state.telefono,
            documentoId: this.state.documentoId,
            curp: this.state.curp,
            edad: this.state.edad,
            ciudad: this.state.ciudad,
            estado: this.state.estado,
            pais: this.state.pais,
            foto: this.state.foto,
            alertas: this.state.alertas,
            fechaNacimiento : anio + '-' + mes + '-' + dia,
            medicoCabecera : (this.state.medicoCabecera)?this.state.medicoCabecera:null,
            sexo: this.state.sexo,
        };
        console.log(data);
        return data;
    }

    render() {
        
        return(
            <div className="area-trabajo-container">
                <div className="action-button-container">
                    <button className="button-cancel" onClick={this.props.cancel}>
                        Cancelar
                    </button>
                    <button className="button-save" onClick={this.guarda}>
                        Guardar
                    </button>
                    
                </div>
                <div className="edicion-container">
                    <div className="edicion-pacientes-generales">
                        <div className="pacientes-foto">
                            <label htmlFor="userfile">
                                <img src={this.state.userfile} alt="Foto de Paciente" className="paciente-foto" />
                            </label>
                            <input id="userfile" type="file" onChange={this.uploadFoto} />
                        </div>
                        <div className="pacientes-datos">
                            <label>Nombre*</label>
                            <input type="text" name="nombre" placeholder="ej. Juan José" onChange={this.handleInputChange} value={this.state.nombre} />
                            <label>Apellidos*</label>
                            <input type="text" name="apellido" placeholder="ej. Lopez Torres" onChange={this.handleInputChange} value={this.state.apellido} />
                            <label>Celular*</label>
                            <input type="text" name="celular" placeholder="ej. 222 123 4567" onChange={this.handleInputChange} value={this.state.celular} />
                            <label>M&eacute;dico</label>
                            <select name="medicoCabecera" onChange={this.handleInputChange} value={this.state.medicoCabecera}>
                                <option value="">Seleccione uno</option>
                                {
                                    this.state.medicos.map(
                                    (item) => <option key={item.id} value={item.id}>{item.nombre + ' ' + item.apellido}</option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                </div>
                <div className="edicion-container edicion-pacientes-generales">
                    <div className="pacientes-datos">
                        <label>Correo</label>
                        <input type="text" name="correo" placeholder="ej. correo@ejemplo.com" onChange={this.handleInputChange} value={this.state.correo} />
                        <label>Alertas</label>
                        <input type="text" name="alertas" placeholder="ej. Diabetico, Hipertenso" onChange={this.handleInputChange} value={this.state.alertas} />
                        <label>Sexo</label>
                        <select name="sexo" onChange={this.handleInputChange} value={this.state.sexo}>
                            <option value="H">Hombre</option>
                            <option value="M">Mujer</option>
                        </select>
                        <label>Fecha de Nacimiento</label>
                        {/* <DatePicker 
                            selected={this.state.fechaNacimiento} 
                            onChange={this.handleChangeFecha} 
                            className="inputs"
                            dateFormat="dd/MM/yyyy"
                            showYearDropdown={true}
                            dropdownMode="scroll"
                            showMonthDropdown={true} /> */}
                        <DatePicker
                            onChange={this.handleChangeFecha}
                            value={this.state.fechaNacimiento}
                            className="inputs"
                            format="dd/MM/y"
                            name="fechaNacimiento"
                        />
                    </div>
                    <div>&nbsp;</div>
                    <div className="pacientes-datos">
                        <label>Domicilio</label>
                        <input type="text" name="domicilio" placeholder="ej. Av. de los Insurgentes Sur 3000" onChange={this.handleInputChange} value={this.state.domicilio} />
                        <label>Ciudad</label>
                        <input type="text" name="ciudad" placeholder="ej. CDMX" onChange={this.handleInputChange} value={this.state.ciudad} />
                        <label>Estado</label>
                        <input type="text" name="estado" placeholder="ej. CDMX" onChange={this.handleInputChange} value={this.state.estado} />
                        <label>Pais</label>
                        <input type="text" name="pais" placeholder="ej. México" onChange={this.handleInputChange} value={this.state.pais} />
                    </div>
                </div>
            </div>
        );
    }

}