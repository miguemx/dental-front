import React from "react";

import {
    toApi, UrlPagosRegistra
} from "../services/constants";

export default class EdicionPagos extends React.Component {

    state = {
        paciente: null,
        medico: null,
        concepto: "",
        subtotal: "",
        impuestos: "",
        status: "PAGADO",
        formaPago: "EFECTIVO"
    }

    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        let medico = localStorage.getItem("medico");
        this.setState({ 
            paciente: this.props.paciente,
            medico: medico
        });
    }

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
            [name]: value
        });
    }

    /**
     * envia la llamada a la API para poder registrar un pago
     */
    registra = async () => {
        let data = this.state;
        let response = await toApi( UrlPagosRegistra, 'post', data);
        if ( response ) {
            alert(response.message);
            this.props.exito();
        }
    }

    render() {
        return(
            <div className="expediente-panel">
                <div className="expedientes-titulo-lista">Registro de Pago</div>
                <div>
                    <input type="text" name="concepto" value={this.state.concepto} onChange={this.handleInputChange} placeholder="Concepto" />
                    <div className="linear-form">
                        <input type="text" name="subtotal" value={this.state.subtotal} onChange={this.handleInputChange} placeholder="Monto" />
                        <input type="text" name="impuestos" value={this.state.impuestos} onChange={this.handleInputChange} placeholder="% Impuesto" />
                        <input type="text" name="total" value={this.state.total} placeholder="Total" readOnly={true} />
                    </div>
                    <button onClick={this.registra}>Agregar Pago</button>
                </div>
            </div>
        );
    }

}