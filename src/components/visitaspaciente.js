import React from "react";

export default class VisitasPacient extends React.Component {

    visitas = () => {
        return(
            <div>
                {this.props.visitas.map( (item) =>  
                    <div key={item.id}>
                        <div>{item.motivo}</div>
                        <div>{item.fecha}</div>
                    </div>
                )}
            </div>
        );
    }

    render() {
        if ( this.props.visitas === null ) return null;
        return(
            <div className="expediente-trabajo">
                <div className="expediente-items">
                    <this.visitas />
                </div>
                <div className="expediente-area">
                    Pantalla de trabajo Visitas
                </div>
            </div>
        );
    }

}