import React from 'react';

import '../styles/Menu.css';

class Menu extends React.Component {

    state ={
        contextUser : false,
    }

    /**
     * abre o cierra el menu contextual de usuario, dependiento de su estado
     */
    toggleUserMenu = () => {
        let contextUser = !this.state.contextUser;
        this.setState({ contextUser });
    }

    /**
     * renderiza un item de menu principal
     * @param {object} propiedades las propiedades que debera tener el item del menu
     */
    itemMenu = (propiedades) => {
        let className = 'menu-item';
        if ( this.props.selected === propiedades.pantalla ) {
            className += ' menu-item-selected'
        }
        return(
            <button onClick={ () => this.props.handleMenu(propiedades.pantalla) } className={className}>
                {propiedades.etiqueta}
            </button>
        );
    }

    /**
     * elimina del local storage el token de sesion y refresca la pantalla
     */
    cerrarSesion = () => {
        localStorage.removeItem('tk');
        window.location.reload(true);
    }

    render() {
        let classNameMeunContextual = ( this.state.contextUser )? "menu-contextual": "hidden";
        return(
            <div className="menu-container">
                <div className="menu-brand">
                    <strong>Dr. Arturo Garc&iacute;a</strong>
                </div>
                <div className="menu-nav">
                    <this.itemMenu pantalla="pacientes" etiqueta="Pacientes" />
                    <this.itemMenu pantalla="agenda" etiqueta="Agenda" />
                    <this.itemMenu pantalla="presupuestos" etiqueta="Presupuestos" />
                    
                </div>
                <div className="menu-profile">
                    <button className="" onClick={this.toggleUserMenu}></button>
                </div>
                <div className={classNameMeunContextual}>
                    <div className="menu-profile">{this.props.sesion.un}</div>
                    <button className="menu-seccion-item-textual" onClick={this.cerrarSesion}>
                        Cerrar Sesi&oacute;n
                    </button>
                </div>
            </div>
        );
    }
}

export default Menu;