import React from "react";
import NumberFormat from 'react-number-format';
import { BuscadorPaciente } from "../components";
import { toApi, UrlCreaPresupuestos } from "../services/constants";

import "../styles/Forms.css";
import "../styles/Presupuesto.css";

export default class EdicionPresupuesto extends React.Component {
    state = {
        paciente: 0,
        nombre: "",
        descripcion: "",

        items: [],
        subtotal: 0.00,
        impuesto: 0,
        total: 0.00,

        concepto: "",
        cantidad: "",
        unitario: "",
    }

    componentDidMount() {
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        let target = event.target;
        let value = target.value;
        let name = target.name;

        if ( name === "impuesto" ) {
            if ( !isNaN(value) ) {
                let total = this.state.subtotal + ( value/100 * this.state.subtotal  );
                this.setState({ total });
            }
            else {
                value = 0;
            }
        }
    
        this.setState({
            [name]: value
        });
    }

    /**
     * agrega un item a la lista del presupuesto
     */
    agregaItem = () => {
        let totalItem = ( parseFloat(this.state.unitario) * parseFloat(this.state.cantidad) );
        let impuesto = parseFloat( this.state.impuesto );
        if ( !isNaN(totalItem) && !isNaN(impuesto) ) {
            if ( this.state.concepto.length > 0 ) {
                let item = {
                    concepto: this.state.concepto,
                    unitario: this.state.unitario,
                    cantidad: this.state.cantidad,
                    total: totalItem
                };
                let items = this.state.items;
                item.key = items.length;
                items.push( item );
                let subtotal = 0;
                items.map( (itemI) => {
                    subtotal += itemI.total;
                    return 0;
                });
                let total = subtotal + ( impuesto/100 * subtotal  );
                let concepto = "";
                let cantidad = "";
                let unitario = "";
                this.setState({ items, subtotal, total, concepto, cantidad, unitario });
                this.concepto.focus();
            }
            else {
                alert( "Por favor verifique que ha escrito un concepto." );
            }
        }
        else {
            alert( "Por favor verifique que la cantidad y el precio unitario sean valores numericos." );
        }
    }

    /**
     * elimina un item de la lista de componentes del presupuesto
     * @param {int} key el ID (posicion) del elemento que se debe quitar
     */
    quitar = (key) => {
        let itemsTemp = this.state.items;
        let items = [];
        let subtotal = 0;
        let total = 0;
        let impuesto = parseFloat( this.state.impuesto );
        for ( let i=0; i<itemsTemp.length; i++ ) {
            if ( itemsTemp[i].key !== key ) {
                itemsTemp[i].key = items.length;
                items.push( itemsTemp[i] );
                subtotal += itemsTemp[i].total;
            }
        }
        total = subtotal + ( impuesto/100 * subtotal  );
        this.setState({ items, subtotal, total });
    }

    /**
     * realiza una llamada a la API para poder guardar el presupuesto
     */
    guarda = async () => {
        if ( this.state.items.length > 0 ) {
            console.log( this.state );
            let data = {
                paciente: this.state.paciente,
                nombre: this.state.nombre,
                status: "SOLICITADO",
                impuesto: this.state.impuesto,
                items: this.state.items
            }
            if ( this.state.descripcion.length > 0 ) {
                data.descripcion = this.state.descripcion;
            }
            let response = await toApi ( UrlCreaPresupuestos, 'post', data );
            if ( response ) {
                alert( response.message );
                this.props.exito();
            }
        }
        else {
            alert( "No se puede agregar el presupuesto si no tiene al menos un concepto." );
        }
    }

    render() {
        return(
            <div className="area-trabajo-container">
                <div className="action-button-container">
                    <button className="button-cancel" onClick={this.props.cancel}>
                        Cancelar
                    </button>
                    <button className="button-save" onClick={this.guarda}>
                        Guardar
                    </button>
                </div>
                <div className="presupuesto-container">
                    <label>Paciente*</label>
                    <BuscadorPaciente onSelect={ (id) => { this.setState({ paciente: id }) }} />

                    <label>T&iacute;tulo del Presupuesto*</label>
                    <input type="text" name="nombre" value={this.state.titulo} onChange={this.handleInputChange} />

                    <label>Descripci&oacute;n</label>
                    <input type="text" name="descripcion" value={this.state.titulo} onChange={this.handleInputChange} />
                    
                    <div className="presupuesto-tabla">
                        <span className="presupuesto-concepto titulos-centrado">
                            <label>Concepto*</label>
                            <input type="text" name="concepto" value={this.state.concepto} onChange={this.handleInputChange} ref={(input) => { this.concepto = input; }} />
                        </span>
                        <span className="presupuesto-cantidad titulos-centrado">
                            <label>Cantidad*</label>
                            <input type="text" name="cantidad" value={this.state.cantidad} onChange={this.handleInputChange} style={{textAlign: "center"}} />
                        </span>
                        <span className="presupuesto-cantidad titulos-centrado">
                            <label>Precio Unitario*</label>
                            <input type="text" name="unitario" value={this.state.unitario} onChange={this.handleInputChange} style={{textAlign: "right"}} />
                        </span>
                        <span className="presupuesto-cantidad titulos-centrado action-button-container">
                            <button className="button-add" onClick={this.agregaItem}>Agregar</button>
                        </span>
                    </div>

                    <table className="presupuesto-items">
                        <thead>
                            <tr>
                                <th className="presupuesto-items-concepto">Concepto</th>
                                <th className="presupuesto-items-cantidades">Cantidad</th>
                                <th className="presupuesto-items-cantidades">Precio Unitario</th>
                                <th className="presupuesto-items-cantidades">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.items.map( (item) => 
                                    <tr key={item.key}>
                                        <td className="registro-small">
                                            <div style={{display: "flex", flexDirection:"row", alignItems:"center"}}>
                                                <button className="button-inline button-delete" onClick={()=>this.quitar(item.key)}></button>
                                                {item.concepto} 
                                            </div>
                                        </td>
                                        <td className="registro-small" style={{textAlign: "center"}}>{item.cantidad}</td>
                                        <td className="registro-small" style={{textAlign: "right"}}>
                                            <NumberFormat
                                                value={item.unitario} 
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'} 
                                                decimalSeparator="." 
                                                decimalScale={2} fixedDecimalScale={true} 
                                            />
                                        </td>
                                        <td className="registro-small" style={{textAlign: "right"}}>
                                            <NumberFormat
                                                value={item.total} 
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'} 
                                                decimalSeparator="." 
                                                decimalScale={2} fixedDecimalScale={true} 
                                            />
                                        </td>
                                        
                                    </tr>
                                )
                            }
                            <tr>
                                <td className="registro-small presupuesto-items-numeros" colSpan={5}>&nbsp;</td>
                            </tr>
                            <tr>
                                <td className="registro-small presupuesto-items-numeros" colSpan={3}>Subtotal</td>
                                <td className="registro-small presupuesto-items-numeros">
                                    <NumberFormat
                                        value={this.state.subtotal} 
                                        displayType={'text'} 
                                        thousandSeparator={true} 
                                        prefix={'$'} 
                                        decimalSeparator="." 
                                        decimalScale={2} fixedDecimalScale={true} 
                                    />
                                </td>
                                
                            </tr>
                            <tr>
                                <td className="registro-small presupuesto-items-numeros" colSpan={3}>% de Impuesto</td>
                                <td className="registro-small presupuesto-items-numeros">
                                    <input type="text" name="impuesto" value={this.state.impuesto} onChange={this.handleInputChange} style={{width: 40, textAlign: "right"}} />
                                </td>
                                
                            </tr>
                            <tr>
                                <td className="registro-small presupuesto-items-numeros" colSpan={3}>Total</td>
                                <td className="registro-small presupuesto-items-numeros">
                                    <NumberFormat
                                        value={this.state.total} 
                                        displayType={'text'} 
                                        thousandSeparator={true} 
                                        prefix={'$'} 
                                        decimalSeparator="." 
                                        decimalScale={2} fixedDecimalScale={true} 
                                    />
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
        );
    }
}