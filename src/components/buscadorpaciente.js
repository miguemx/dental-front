import React from "react";
import Search from "react-search";

import { UrlListaPacientes, getHeaders } from "../services/constants";

export default class BuscadorPaciente extends React.Component {

    state = {
        pacientes : [
            { id: 0, value: "Pacientes no encontrados" }
        ],
        paciente: '',
        letras:'',
        id: ''
    }

    componentDidMount() {
        this.getPacientes();
    }

    /**
     * obtiene desde la aPI la lista de pacientes que corresponden al medico actual
     */
    getPacientes = async () => {
        let medico = localStorage.getItem('medico');
        let data = { pagina: 1, registros: 999, filtros: { medicoCabecera: medico } };
        let token = localStorage.getItem('tk');
        let responseObject;
        let response = await fetch(UrlListaPacientes, {
            method: 'POST', 
            body: JSON.stringify(data), 
			headers: getHeaders(token),
		}).then( (res) => { responseObject=res; return res.json() } ).catch(error => console.error('Error:', error))
		.catch(error => console.error('Error:', error))
        .then( (response) => { return response } );
        if ( typeof(response) === 'object' ) {
            if ( response.status === 'ok' ) {
                localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
                this.creaListaPacientes( response.data.registros );
            }
            else {
                if ( response.message.startsWith("401") ) {
                    window.location.reload(true);
                }
            }
        }
    }

    creaListaPacientes = (registros) => {
        let pacientes = registros.map( (item) => { return { 
            id: parseInt(item.id),
            value: item.nombre + ' ' + item.apellido
        } } );
        this.setState({ pacientes });
    }

    /**
     * envia al elemento padre el dato seleccionado
     * @param {object} data la informacion que tiene el registro rendeizado por el listado de pacientes
     */
    selectPaciente = (data) => {
        if ( data.length > 0 ) {
            this.props.onSelect( data[0].id );
        }
        else {
            this.props.onSelect( null );
        }
    }

    render() {
        return(
            <Search items={this.state.pacientes}
                placeholder="Seleccione un paciente"
                maxSelected={1}
                multiple={false}
                onItemsChanged={this.selectPaciente.bind(this)} />
      
        );
    }

}