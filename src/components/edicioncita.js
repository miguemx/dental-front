import React from "react";
import DatePicker from 'react-datepicker';
import moment from "moment";
import { BuscadorPaciente } from "../components";
import { UrlAgendaRegistra, toApi, horaExtractor, fechaExtractor, UrlAgendaActualiza } from "../services/constants"

/**
 * Clase para el componente de edicion de citas
 * @property {function} cancel una funcion para ejecutar despues de la accion de cancelar
 * @property {function} exito una funcion para ejecutar si se guarda correctamente la cuta
 * @property {string} inicio la cadena con la fecha de inicio, en formato Y-m-d H:i:s; puede ser null
 * @property {string} fin la cadena con la fecha de fin, en formato Y-m-d H:i:s; puede ser null
 * @property {object} data (opcional) un objeto con los datos del estado a settear
 */
export default class EdicionCita extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            paciente: '',
            motivo: '',
            duracion: '',
            notas: '',
            fecha: new Date(),
            horaInicio:'',
            horaFin:'',
            id: null,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        // let medico = localStorage.getItem('medico');
        this.setInitialTimes();
        this.setState({ paciente: '1'});
        if ( this.props.cita !== null ) {
            this.llenaDatosCita();
        }
    }

    /**
     * ajusta las horas iniciales para poder crear una cita
     */
    setInitialTimes = () => {
        let fecha = new Date();
        let horaInicio = '08:00';
        let horaFin = '08:30'
        if ( this.props.inicio !== null ) {
            let fin = this.props.inicio;
            
            fecha = this.props.inicio;
            horaInicio = ( (fecha.getHours()<10)? '0'+fecha.getHours(): fecha.getHours() ) + ':';
            horaInicio += ( (fecha.getMinutes()<10)? '0'+fecha.getMinutes(): fecha.getMinutes() );
            if ( this.props.fin !== null ) {
                fin = this.props.fin;
            }
            else {
                let momento = moment(fecha);
                momento.add(30,'minutes');
                fin = momento.toDate();
            }
            horaFin = ( (fin.getHours()<10)? '0'+fin.getHours(): fin.getHours() ) + ':';
            horaFin += ( (fin.getMinutes()<10)? '0'+fin.getMinutes(): fin.getMinutes() );
        }
        
        this.setState({ fecha, horaInicio, horaFin });
    }

    /**
     * obtiene dese la propiedad "cita" los datos para llenar el formulario de edicion
     */
    llenaDatosCita = () => {
        let fecha = fechaExtractor( this.props.cita.fecha );
        if ( fecha === null ) {
            fecha = new Date();
        }

        this.setState({
            id: this.props.cita.id,
            paciente: this.props.cita.paciente.id,
            motivo: this.props.cita.motivo,
            duracion: '',
            notas: this.props.cita.notas,
            fecha: fecha,
            horaInicio:horaExtractor(this.props.cita.fecha),
            horaFin:horaExtractor(this.props.cita.fechaFin),
        });
    }

    /**
     * pone en el estado, el valor del campo de formulario que esta siendo afectado
     * @param {object} event el evento que dispara el cambio de estado de un control 
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
            [name]: value
        });
    }

    /**
     * envia a la API los datos para agegar una nueva entrada en la agenda
     */
    guarda = async () => {
        let stFecha = this.state.fecha;
        let fecha = stFecha.getFullYear() + "-" + ( (stFecha.getMonth()<9)? "0"+(stFecha.getMonth()+1): stFecha.getMonth()+1 ) + "-" + ( (stFecha.getDate()<9)? "0"+(stFecha.getDate()): stFecha.getDate() );
        let fechaInicio = fecha + ' ' + this.state.horaInicio + ':00';
        let fechaFin = fecha + ' ' + this.state.horaFin + ':00';
        let data = {
            medico: localStorage.getItem("medico"),
            paciente: this.state.paciente,
            motivo: this.state.motivo,
            fecha: fechaInicio,
            fechaFin: fechaFin,
            status: "EN ESPERA",
            notas: this.state.notas
        }
        let response = null;
        if( this.state.id === null ) {
            response = await toApi( UrlAgendaRegistra, "post", data );
        }
        else {
            response = await toApi( UrlAgendaActualiza + this.state.id, "post", data );
        }
        
        if ( response !== null ) {
            alert( response.message );
            this.props.exito(response.data.id);
        }
    }

    horaSelector = (stateNameHora) => {
        let name = stateNameHora.stateNameHora;
        let horas = [ '00:00',
            '00:30', '01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00',
            '07:30', '08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00',
            '14:30', '15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00',
            '21:30', '22:00','22:30','23:00','23:30',
        ];
        return(
            <div className="form-row">
                <select name={name} onChange={this.handleInputChange} value={this.state[name]}>
                    {
                        horas.map(
                            (hora) => <option key={hora} value={hora}>{hora}</option>
                        )
                    }
                </select>
            </div>
        );
    }

    render() {
        return(
            <div className="area-trabajo-container">
                <div className="action-button-container">
                    {
                        (this.props.cancel !== null )?
                        <button className="button-cancel" onClick={this.props.cancel}>
                            Cancelar
                        </button>
                        :
                        null
                    }
                    
                    <button className="button-save" onClick={this.guarda}>
                        Guardar
                    </button>
                </div>

                <div className="formulario-dinamico">
                    <label>Motivo*</label>
                    <input type="text" name="motivo" placeholder="ej. Limpieza" onChange={this.handleInputChange} value={this.state.motivo} />

                    <label>Paciente*</label>
                    {
                        (this.props.cita === null)?
                        <BuscadorPaciente onSelect={ (id) => { this.setState({ paciente: id }) }} />
                        :
                        <div className="inputs inputs-disabled">{this.props.cita.paciente.nombre + " " + this.props.cita.paciente.apellido}</div>
                    }
                    
                    
                    <div className="form-row">
                        <div>
                            <label>Fecha*</label>
                            <DatePicker 
                                selected={this.state.fecha} 
                                onChange={(fecha) => this.setState({fecha})} 
                                className="inputs"
                                dateFormat="yyyy-MM-dd"
                                showYearDropdown={true}
                                dropdownMode="scroll"
                                showMonthDropdown={true} />
                        </div>
                   
                        <div>
                            <label>Hora de Inicio*</label>
                            <this.horaSelector stateNameHora="horaInicio" />
                        </div>

                        <div>
                            <label>Hora de Salida*</label>
                            <this.horaSelector stateNameHora="horaFin" />
                        </div>
                    </div>
                    
                    <label>Notas</label>
                    <textarea name="notas" value={this.state.notas} onChange={this.handleInputChange} />

                    
                </div>

                
            </div>
        );
    }
}