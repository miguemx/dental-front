// const dominio = 'http://148.228.11.51/sgd/public/';
// const dominioPublico = 'http://148.228.11.51/sgd/public/'

// const dominio = 'http://sgd.privatecarservise.com/';
// const dominioPublico = 'http://sgd.privatecarservise.com/'

const dominio = 'http://localhost/dental-api/public/';
const dominioPublico = 'http://localhost/dental-api/public/'

export const UrlLogin = dominio + 'Users/Login';
export const UrlToken = dominio + 'Users/CheckToken';

export const UrlPFotos = dominioPublico + 'pphotos/';
export const UrlCargaFotos = dominio + 'Uploads/Fotos';

export const UrlListaPacientes = dominio + 'Pacientes';
export const UrlVerPaciente = dominio + 'Pacientes/Ver/';
export const UrlAgregaPaciente = dominio + 'Pacientes/Crea';
export const UrlEditaPaciente = dominio + 'Pacientes/Editar';
export const UrlEliminaPaciente = dominio + 'Pacientes/Eliminar';

export const UrlListaMedicos = dominio + 'Medicos';

export const UrlAgendaRegistra = dominio + "Agenda/Registra";
export const UrlAgendaActualiza = dominio + "Agenda/Edita/";
export const UrlAgendaMedico = dominio + "Agenda/Medico/";
export const UrlAgendaPaciente = dominio + "Agenda/Paciente/";

export const UrlListaPresupuestos = dominio + 'Presupuestos';
export const UrlCreaPresupuestos = dominio + 'Presupuestos/Crea';
export const UrlPresupuestosVer = dominio + "Presupuestos/Ver/";
export const UrlPresupuestosEditar = dominio + "Presupuestos/Editar/";

export const UrlPagosRegistra = dominio + "Pagos/Registra";
export const UrlPagosSaldo = dominio + "Pagos/Saldo/";



export const fotoPacienteGenerica = require('../styles/icons/abstract-user-flat-3.png');

export var getHeaders = (token) => {
    let headers = {
        'Content-Type':'application/json',
        'Accept':  '*/*',
        'Access-Control-Request-Headers' : 'xapiauth',
        'xapiauth': token
    }
    return headers;
}

/**
 * envia una peticion a la API y regresa la respuesta de la misma
 * @param {string} url 
 * @param {string} method 
 * @param {object} parameters 
 * @return {object} data los datos obtenidos desde la api
 * @return {null} nulo si existe algun error; en caso de ser sesion expirada, refrescara la pagina
 */
export var toApi = async ( url, method, parameters ) => {
    let token = localStorage.getItem('tk');
    let responseObject;
    let options = {
        method: method, 
        body: JSON.stringify(parameters), 
        headers: getHeaders(token),
    };
    if ( method.toLowerCase() === 'get' ) {
        options = { method: method, headers: getHeaders(token) };
    }
    let response = await fetch(url, options).then( (res) => { responseObject=res; return res.json() } ).catch(error => console.error('Error:', error))
    .catch(error => console.error('Error:', error))
    .then( (response) => { return response } );
    if ( typeof(response) === 'object' ) {
        if ( response.status === 'ok' ) {
            localStorage.setItem('tk', responseObject.headers.get('xapiauth'));
            return response;
        }
        else {
            switch ( response.code ) {
                case "400": notificaErrores(response.data); break;
                case "401": window.location.reload(true); break;
                default: alert( response.message ); break;
            }
        }
    }
    return null;
}

/**
 * obtien un objeto de tipo Date a partir de una cadena de mysql
 * @param {string} cadena la cadena con fecha y hora en formato Y-m-d H:i:s
 * @return {date} fecha creada a partir de la cadena; null en caso de error
 */
export const fechaExtractor = (cadena) => {
    let fecha = null;
    try {
        let extractor = cadena.split(" "); // separar la fecha de la hora
        let fechaData = extractor[0].split("-");
        fecha = new Date(parseInt(fechaData[0]), parseInt(fechaData[1])-1, parseInt(fechaData[2]));
    }
    catch {
        console.log("Excepetion in fecha extractor.");
    }
    return fecha;
}

/**
 * obtien una cadena con formato de hora sin segundos a partir de una fecha MySQL
 * @param {string} cadena la cadena con fecha y hora en formato Y-m-d H:i:s
 * @return {string} hora creada a partir de la cadena; null en caso de error
 */
export const horaExtractor = (cadena) => {
    let hora = null;
    try {
        let extractor = cadena.split(" "); // separar la fecha de la hora
        hora = extractor[1].substring(0,5);
    }
    catch {
        console.log("Excepetion in fecha extractor.");
    }
    return hora;
}


/**
 * manda un alert con la lista de errores regresados por la api
 * @param {array} arrayErrores 
 */
function notificaErrores(arrayErrores) {
    let message = "Por favor corrija los siguiente errores: \n\n";
    for( let i=0; i<arrayErrores.length; i++ ) {
        message += arrayErrores[i] + "\n";
    }

    alert ( message );
}